{{ if data:instagram }}
<a href="{{ data:instagram }}" target="_blank"><i class="fa fa-instagram"></i></a>
{{endif}}
{{ if data:facebook }}
<a href="{{data:facebook}}" target="_blank"><i class="fa fa-facebook"></i></a>
{{endif}}
{{ if data:pinterest }}
<a href="{{data:pinterest}}" target="_blank"><i class="fa fa-pinterest-p"></i></a>
{{endif}}
<a href="callto:{{data:phone}}" class="tooltipped" data-position="left" data-delay="50" data-tooltip="Contáctenos"><i class="fa fa-phone"></i></a>


