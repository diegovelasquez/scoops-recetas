<div id="baseurl" class="hide">{{ url:site }}</div>
<div id="banner"></div>
<section class="banner wow fadeIn">
    <div class="icon wow bounceInRight "></div>
    {{ if banner }}
    <div class="bxslider">
        {{ banner }}
        <div class="mySlide" style="background-image: url('{{image}}')">
            <div class="container relative">
                <div class="caption">
                    <div class="prev"><i class="fa fa-chevron-up"></i>
                    </div>
                    <div class="next"><i class="fa fa-chevron-down"></i>
                    </div>
                    <h2 class="wow fadeInDown">{{title}}</h2>
                </div>
            </div>
        </div>
        {{ /banner }}
    </div>
    {{ endif }}
</section>
<section class="logos" id="about">
    <div class="container">
        <div class="row">
            <div class="col s12 m6 wow fadeInLeft">
                <figure>
                    <img src="{{destacado:image}}" alt="scoops">
                </figure>
            </div>
            <div class="col s12 m6 wow fadeInRight">
                <p>{{destacado:text}}</p>
            </div>
        </div>
    </div>
</section>
<section class="carousel" id="marca">
    <div class="container relative">
        <div class="row">
            <div class="col s12 wow fadeInDown">
                <h2>{{linkintro:title}}</h2>
            </div>
        </div>
    </div>
    <div class="col s12 l7 hide">
        <div class="text">
            <ul>
                {{ people }}
                <li>
                    <div class="icon">
                        <img src="addons/shared_addons/themes/picasso/img/icono{{par}}.png" alt="{{title}}">
                    </div>
                    <h3>{{ title }}</h3>
                    {{ text }}
                </li>
                {{ /people }}
            </ul>
        </div>
    </div>
    <img title="{{linkintro:title}}" src="{{linkintro:image}}">


</section>
<section class="logos af" id="owned">
    <div class="container">
        <div class="cite lined wow fadeInDown clear">
            {{video:title}}
        </div>
        <div class="row">
            <div class="col s12 wow fadeInUp">
                {{ customers }}
                <article>
                    <figure>
                        <img src="{{image}}" alt="{{name}}">
                        <div class="table">
                            <img src="{{image2}}" alt="{{name}}">
                        </div>
                    </figure>
                    <h3>{{ name }}</h3>
                    <p> {{text}} </p>
                </article>
                {{ /customers }}
            </div>
        </div>
    </div>
</section>
<section class="data logos af" id="cite">
    <div class="container">
        <div class="cite lined wow fadeInDown clear">
            {{linkintro:title2}}
        </div>
        <div class="row">
            <div class="col s12">

                <ul class="collapsible" data-collapsible="accordion">
                    {{ contact }}
                    <li>
                        <div class="collapsible-header"><i class="fa fa-globe"></i>{{ city }}</div>
                        <div class="collapsible-body">
                            {{ places }}
                            <p><i class="fa fa-angle-double-right"></i> <a href="{{ link }}" target="_blank"> {{ title }}</a></p>
                            {{ /places }}
                        </div>
                    </li>
                    {{ /contact }}
                </ul>                
            </div>
        </div>
    </div>
</section>
<section class="formu logos af" id="contact">
    <div class="container">
        <div class="cite lined wow fadeInDown clear">
            {{linkintro:title3}}
        </div>
        <div class="row">
            <div class="col s12">
                <div id="loading_contacts"></div>
                <div class="s2"></div>    
                <?php echo form_open(site_url('contact_us/send'), 'class="crud form_contac_ajax"'); ?>
                <div class="input-field col s12 m6">
                    <input class="browser-default" id="nombre" type="text" class="validate name" name="name" value="<?php echo set_value('nombre') ?>">
                    <label for="nombre">Tu Nombre</label>
                </div>
                <div class="input-field col s12 m6">
                    <input class="browser-default" id="email" type="text" class="validate email" name="email" value="<?php echo set_value('correo') ?>">
                    <label for="email">Tu Email</label>
                </div>
                <div class="input-field col s12 m6">
                    <input class="browser-default" id="phone" type="text" class="validate phone" name="phone" value="<?php echo set_value('telefono') ?>">
                    <label for="phone">Tu Teléfono</label>
                </div>
                <div class="input-field col s12 m6">
                    <input class="browser-default" id="city" type="text" class="validate city" name="city" value="<?php echo set_value('city') ?>">
                    <label for="city">Tu Ciudad</label>
                </div>
                <div class="input-field col s12">
                    <textarea class="browser-default" id="textarea1" name="message" class="materialize-textarea message"></textarea>
                    <label for="textarea1">Mensaje</label>
                </div>
                <i class="waves-effect waves-light btn waves-input-wrapper" style=""><input type="submit" class="waves-button-input" name="btnAction" value="Enviar"></i>
                <div class="clear"></div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</section>
