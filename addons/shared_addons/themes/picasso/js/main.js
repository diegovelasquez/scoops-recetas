$(document).ready(function () {
    $(function () {
        // init Isotope
        var $grid = $('.grid').isotope({
            itemSelector: '.element-item',
            layoutMode: 'fitRows'
        });
        // filter functions
        var filterFns = {
            // show if number is greater than 50
            numberGreaterThan50: function () {
                var number = $(this).find('.number').text();
                return parseInt(number, 10) > 50;
            },
            // show if name ends with -ium
            ium: function () {
                var name = $(this).find('.name').text();
                return name.match(/ium$/);
            }
        };
        // bind filter button click
        $('.filters-button-group').on('click', 'button', function () {
            var filterValue = $(this).attr('data-filter');
            // use filterFn if matches value
            filterValue = filterFns[filterValue] || filterValue;
            $grid.isotope({
                filter: filterValue
            });
        });
        // change is-checked class on buttons
        $('.button-group').each(function (i, buttonGroup) {
            var $buttonGroup = $(buttonGroup);
            $buttonGroup.on('click', 'button', function () {
                $buttonGroup.find('.is-checked').removeClass('is-checked');
                $(this).addClass('is-checked');
            });
        });

    });

    $('section.areas article:eq(0), section.areas article:eq(2)').addClass('fadeInLeft')
    $('section.areas article:eq(1), section.areas article:eq(3)').addClass('fadeInRight')


    $('section.areas article:eq(1) .col.l5').insertAfter('section.areas article:eq(1) .col.l7')
    $('section.areas article:eq(3) .col.l5').insertAfter('section.areas article:eq(3) .col.l7')


//$(function() {
//  $('#consultant').change(function(){
//    $('.colors').hide();
//				$('.consultant').removeClass('consultant-active')
//    $('#' + $(this).val()).addClass('consultant-active')
//
//  });
//});


    $(document).scroll(function () {
        console.log($(document).scrollTop());
    })

    var url_completa = location.href;
    $("nav li a[href='" + url_completa + "']").addClass("activo");

    $(function () {

        $(' #da-thumbs .myBox ').each(function () {
            $(this).hoverdir();

        });

    });

// 	$('.da-thumbs .theBox').hover(function () {
// 		$(this).children('.myBox').next('.myText').toggleClass('myText-active')
// 		return false;
// 	})

    $('section.outstanding article').hover(function () {
        $(this).children('.caption').toggleClass('caption-active')
        return false;
    })

    $('.next-form').click(function () {
        $('.form.step-1').removeClass('form-active')
        $('.form.step-2').addClass('form-active')
        return false;
    })

//		


    //@Tabs

    $('#video').click(function () {
        $(this).get(0).paused ? $(this).get(0).play() : $(this).get(0).pause();
    });


    $('#news').bxSlider({
        minSlides: 1,
        maxSlides: 1,
        slideMargin: 0
    });

    $('.myTabs').easyResponsiveTabs({
        type: 'vertical',
        width: 'auto',
        fit: true,
        tabidentify: 'tab',
    });


    $(".button-collapse").sideNav();
    $('.slider').slider({
        interval: 5000,
        indicators: true,
        full_width: true

    });
    //	$('.highlight article').hover(function () {
    //		$(this).toggleClass('z-depth-4')
    //		return false;
    //	})
    //	
    //		$('.card').on('mouseenter', function () {
    //		$(this).toggleClass('z-depth-3')
    //		return false;
    //	})
    $('.modal-trigger').leanModal();
// 	$('.dropdown-button').dropdown({
// 		inDuration: 300,
// 		outDuration: 225,
// 		constrain_width: false, // Does not change width of dropdown to that of the activator
// 		hover: true, // Activate on hover
// 		gutter: 0, // Spacing from edge
// 		belowOrigin: true // Displays dropdown below the button
// 	});


    $('.submenu-open').hover(function () {
        $(this).nextAll('.submenu-open').children('.submenu').removeClass('submenu-active')
        $(this).prevAll('.submenu-open').children('.submenu').removeClass('submenu-active')
        $(this).children('.submenu').addClass('submenu-active')
        return false;
    })

    $(".banner, main h1").hover(function () {
        $('.submenu').removeClass('submenu-active')
        return false;
    });



    //	$(".fancybox").fancybox({
    //		openEffect: 'none',
    //		closeEffect: 'none'
    //	});
    $(".fancybox-thumb").fancybox({
        prevEffect: 'none',
        nextEffect: 'none',
        helpers: {
            title: {
                type: 'outside'
            },
            thumbs: {
                width: 50,
                height: 50
            }
        }
    });


    $('.bxslider').bxSlider({
        mode: 'fade',
        nextSelector: '.banner .next',
        prevSelector: '.banner .prev',
        nextText: '',
        prevText: '',
        useCSS: false,
        infiniteLoop: true,
        hideControlOnEnd: true,
        easing: 'easeInBack',
        speed: 1000,
        auto: true,
    });

    $('.bxslider2').bxSlider({
        mode: 'fade',
        auto: true,
        pause: 4500,
        pager: false
    });
    $('.bxdress').bxSlider({
        slideWidth: 1920,
        minSlides: 3,
        maxSlides: 3,
        moveSlides: 1,
        auto: true,
        pause: 4500,
        pager : false,
        nextSelector : '.bxctr',
        prevSelector : '.bxctr',
        nextText : '',
        prevText : '',
        moveSlides : 1,
        autoHover: true
    });

    $('.got').click(function (e) {
        e.preventDefault();
        var g = $(this).attr('href'),
            of = $(g).offset().top;
        $('body').animate({
            scrollTop: of - 80
        }, 1000)
    })

    $('#carousel').bxSlider({
        slideWidth: 210,
        minSlides: 2,
        maxSlides: 4,
        moveSlides: 1,
        slideMargin: 15,
        auto: true,
        pause: 6000
    });

    var wd = $(window).width();
    if (wd < 600) {



    } else if (wd > 599) {
        $('.highlight.myAccordeon article').click(function () {
            $(this).addClass('myAccordeon-active')
            $(this).prevAll('article').removeClass('myAccordeon-active')
            $(this).nextAll('article').removeClass('myAccordeon-active')
        });

    }


});
$(window).load(function () {
    new WOW().init();
    $('.parallax').parallax();

    var header = $('header');

    $(window).scroll(function () {
        if ($(this).scrollTop() > 140) {
            header.addClass("myHeader");
        } else {
            header.removeClass("myHeader");
        }
    });

    //	$('.preload').fadeOut(300);
// 	 var nav = $('header');
// 	
// 	    $(window).scroll(function () {
// 	        if ($(this).scrollTop() < 560) {
// 	            nav.addClass("fixed-menu");
// 	        } else {
// 	            nav.removeClass("fixed-menu");
// 	        }
// 	    });
//	 
//	  	 var nav2 = $('nav');
// 	
// 	    $(window).scroll(function () {
// 	        if ($(this).scrollTop() < 560) {
// 	            nav2.addClass("fixed-nav");
// 	        } else {
// 	            nav2.removeClass("fixed-nav");
// 	        }
// 	    });
});