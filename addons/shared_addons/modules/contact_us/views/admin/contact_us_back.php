<div id="baseurl" class="hide"><?php echo site_url(); ?></div>
<section class="item">
    <section class="title">
        <h4><?php echo lang('language:title') ?></h4>
    </section>
    <div class="content">
        <div class="tabs">
            <ul class="tab-menu">
                <li><a href="#page-view"><span><?php echo lang('language:data') ?></span></a></li>
                <li><a href="#page-details"><span><?php echo lang('language:manage_data') ?></span></a></li>
                <li><a href="#page-google_maps"><span>Mapa</span></a></li>
            </ul>

            <div class="form_inputs" id="page-view">
                <div class="inline-form">
                    <fieldset>
                        <ul>
                            <li>
                                <label for="name">Introducción de ShowRoom</label>
                                <div class="input"><?php echo isset($data->intro) ? $data->intro : "" ?></div>
                            </li>
                            <li>
                                <label for="name">Introducción de Contacto</label>
                                <div class="input"><?php echo isset($data->footer) ? $data->footer : "" ?></div>
                            </li>
                            <li>
                                <label for="name">Facebook</label>
                                <div class="input"><?php echo isset($data->facebook) ? $data->facebook : "" ?></div>
                            </li>
                            <li>
                                <label for="name">Instagram</label>
                                <div class="input"><?php echo isset($data->instagram) ? $data->instagram : "" ?></div>
                            </li>
                            <li>
                                <label for="name">Pinterest</label>
                                <div class="input"><?php echo isset($data->pinterest) ? $data->pinterest : "" ?></div>
                            </li>
                            <li>
                                <label for="name"><?php echo lang('language:address') ?></label>
                                <div class="input"><?php echo isset($data->adress) ? $data->adress : "" ?></div>
                            </li>
                            <li>
                                <label for="name"><?php echo lang('language:phone') ?></label>
                                <div class="input"><?php echo isset($data->phone) ? $data->phone : "" ?></div>
                            </li>
                            <li>
                                <label for="name"><?php echo lang('language:email') ?> de contacto</label>
                                <div class="input"><?php echo isset($data->email) ? $data->email : "" ?></div>
                            </li>
                            <li>
                                <label for="name">Cita previa</label>
                                <div class="input">
                                    <?php echo isset($data->map) ? $data->map : "" ?>
                                </div>
                            </li>
                            <li>
                                <label for="name">Parqueadero</label>
                                <div class="input">
                                    <?php echo isset($data->schedule) ? $data->schedule : "" ?>
                                </div>
                            </li>
                        </ul>
                    </fieldset>
                </div>
            </div>
            
            <div class="form_inputs" id="page-details">
                <?php echo form_open(site_url('admin/contact_us/index/' . $lang_admin), 'id="form-wysiwyg"'); ?>
                <div class="inline-form">
                    <fieldset>
                        Las Direcciones Url es recomendable que siempre lleven el https:// Como por ejemplo https://www.facebook.com/ <br/><br/><br/>
                        <ul>
                            <li>
                                <label for="name">Introducción de ShowRoom</label>
                                <div class="input">
                                    <textarea name="intro" class="wysiwyg-simple"><?php echo isset($data->intro) ? $data->intro : "";?></textarea>
                                </div>
                            </li>
                            <li>
                                <label for="name">Introducción de Contacto</label>
                                <div class="input">
                                    <textarea name="footer" class="wysiwyg-simple"><?php echo isset($data->footer) ? $data->footer : "";?></textarea>
                                </div>
                            </li>
                            <li>
                                <label for="name">Facebook</label>
                                <div class="input"><?php echo form_input('facebook', set_value('facebook', isset($data->facebook) ? $data->facebook : ""), ' id="facebook"'); ?></div>
                            </li>
                            <li>
                                <label for="name">Instagram</label>
                                <div class="input"><?php echo form_input('instagram', set_value('instagram', isset($data->instagram) ? $data->instagram : ""), ' id="instagram"'); ?></div>
                            </li>
                            <li>
                                <label for="name">Pinterest</label>
                                <div class="input"><?php echo form_input('pinterest', set_value('pinterest', isset($data->pinterest) ? $data->pinterest : ""), ' id="pinterest"'); ?></div>
                            </li>
                            <li>
                                <label for="name"><?php echo lang('language:address') ?></label>
                                <div class="input"><?php echo form_input('adress', set_value('adress', isset($data->adress) ? $data->adress : ""), ' id="adress"'); ?></div>
                            </li>
                            <li>
                                <label for="name"><?php echo lang('language:phone') ?></label>
                                <div class="input"><?php echo form_input('phone', set_value('phone', isset($data->phone) ? $data->phone : ""), ' id="phone"'); ?></div>
                            </li>
                            <li>
                                <label for="name"><?php echo lang('language:email') ?> de contacto</label>
                                <div class="input"><?php echo form_input('email', set_value('correo', isset($data->email) ? $data->email : ""), ' id="email"'); ?></div>
                            </li>
                            <li>
                                <label for="name">Cita previa</label>
                                <div class="input">
                                    <textarea name="map" class="wysiwyg-simple"><?php echo isset($data->map) ? $data->map : "";?></textarea>
                                </div>
                            </li>
                            <li>
                                <label for="name">Parqueadero</label>
                                <div class="input">
                                    <textarea name="schedule" class="wysiwyg-simple">
                                        <?php echo isset($data->schedule) ? $data->schedule : "";?>
                                    </textarea>
                                </div>
                            </li>
                            <li>
                                <div class="buttons float-right padding-top">
                                    <?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))); ?>
                                </div>
                            </li>
                        </ul>
                    </fieldset>
                    <?php echo form_close(); ?>
                </div>
            </div>
            
            <div class="form_inputs" id="page-google_maps">
                <fieldset>

                    <?php // echo anchor('admin/contact_us/create/' . $lang_admin, '<span>+ Nuevo Mapa de google</span>', 'class="btn blue"'); ?>
                    <br>
                    <br>

                    <?php if (!empty($google_maps)): ?>

                        <table border="0" class="table-list" cellspacing="0">
                            <thead>
                                <tr>
                                    <th style="width: 15%">Logitud</th>
                                    <th style="width: 15%">Latitud</th>
                                    <th style="width: 20%">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($google_maps as $google_map): ?>
                                    <tr>
                                        <td><?php echo $google_map->coordinate1 ?></td>
                                        <td><?php echo $google_map->coordinate2 ?></td>
                                        <td>
                                            <?php echo anchor('admin/contact_us/edit/' . $google_map->id . '/' . $lang_admin, lang('global:edit'), 'class="btn green small"'); ?>
                                            <?php // echo anchor('admin/contact_us/destroy/' . $google_map->id . '/' . $lang_admin, lang('global:delete'), array('class' => 'btn red small confirm button')) ?>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>

                    <?php else: ?>
                        <p style="text-align: center">No hay Registros actualmente</p>
                    <?php endif ?>
                </fieldset>
            </div>
            
        </div>
    </div>
</section>