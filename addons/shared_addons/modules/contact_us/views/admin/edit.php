<section class="title">
    <h4>Mapas de google</h4>
</section>
<section class="item">
    <div class="content">
        <div class="tabs">
            <ul class="tab-menu">
                <li><a href="#page-google_map"><span>Editar Mapa de google</span></a></li>
            </ul>
            <div class="form_inputs" id="page-google_map">
                <?php echo form_open_multipart(site_url('admin/contact_us/update/' . $lang_admin), 'id="form-wysiwyg"'); ?>
                <div class="inline-form">
                    <fieldset>
                        <ul>
                            <li class="hide">
                                <label for="name">Imagen
                                    <small>
                                        - Imagen Permitidas gif | jpg | png | jpeg<br>
                                    </small>
                                </label>
                                <div class="input">
                                    <?php if (!empty($google_map->image)): ?>
                                        <div>
                                            <img src="<?php echo val_image($google_map->image) ?>" width="298">
                                        </div>
                                    <?php endif; ?>
                                    <div class="btn-false">
                                        <div class="btn">Examinar</div>
                                        <?php echo form_upload('image', '', ' id="image"'); ?>
                                    </div>
                                </div>
                                <br class="clear">
                            </li>
                            <li class="hide">
                                <label for="title">Dirección <span>*</span></label>
                                <div class="input"><?php echo form_input('adress', $google_map->adress, 'class="dev-input-title"'); ?></div>
                            </li>
                            <li class="hide">
                                <label for="title">Nombre <span>*</span></label>
                                <div class="input"><?php echo form_input('name', $google_map->name, 'class="dev-input-title"'); ?></div>
                            </li>
                            <li class="hide">
                                <label for="path">Categorias</label>
                                <select name="categories[]" multiple>
                                    <option value="1" selected>Seleccione una Opción</option>
                                </select>
                            </li>
                            <li class="hide">
                                <label for="introduction">Descripción
                                    <span>*</span>
                                    <small class="counter-text"></small>
                                </label>
                                <div class="input"><?php echo form_textarea('description', $google_map->description, 'class="dev-input-textarea limit-text"'); ?></div>
                            </li>
                            <li class="even hide">
                                <label for="name">
                                    Horario
                                    <span>*</span>
                                </label>
                                <div class="input"><?php echo form_textarea('schedule', $google_map->schedule, 'class="dev-input-textarea limit-text"'); ?></div>
                                <br class="clear">
                            </li>
                            <li>
                                <label for="title">Mapa</label>
                                <div class="map_canvas"></div>
                            </li>
                            <li>
                                <label for="title">Buscador</label>
                                <input id="geocomplete" type="text" placeholder="buscar" value="" />
                                <input id="find" type="button" value="Buscar" />
                            </li>
                            <li>
                                <label for="title">Cordinada 1 <span>*</span></label>
                                <div class="input"><?php echo form_input('coordinate1', $google_map->coordinate1, 'id="lat" class="dev-input-title"'); ?></div>
                            </li>
                            <li>
                                <label for="title">Cordinada 2 <span>*</span></label>
                                <div class="input"><?php echo form_input('coordinate2', $google_map->coordinate2, 'id="lng" class="dev-input-title"'); ?></div>
                            </li>
                        </ul>
                    </fieldset>

                    <div class="buttons float-right padding-top">
                        <?php echo form_hidden('id', $google_map->id); ?>
                        <?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))); ?>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>

        </div>
    </div>
</section>