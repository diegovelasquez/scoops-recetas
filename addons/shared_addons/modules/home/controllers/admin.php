<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @author 	Luis Fernando Salazar
 * @author 	Brayan Acebo
 * @package 	PyroCMS
 * @subpackage 	Home Module
 * @category 	Modulos
 * @license 	Apache License v2.0
 */
class Admin extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        // cargamos el lenguaje
        $this->lang->load('home');
        // añadimos atributos al template
        $this->template
                ->append_js('module::developer.js')
                ->append_metadata($this->load->view('fragments/wysiwyg', null, TRUE));
    }

    public function index($lang_admin = 'es', $ordering = 0) {
        $ordering = (empty($ordering) ? FALSE : TRUE);
        // consultamos los datos del Banner
        $banner = $this->db->where('lang', $lang_admin)->get('home_banner')->result();
        //Consultamos los datos del texto informativo
        $linkintro = $this->db->where('lang', $lang_admin)->get('home_links_intro')->row();
        $text_info = $this->db->where('id', 1)->get('home_text_info')->row();
        // Consultamos los datos del slider de Clientes
        $people = $this->db->where('lang', $lang_admin)->get('people')->result();
        // Consultamos los datos del slider de Clientes
        $customers = $this->db->get('home_customers')->result();
        // consultamos los datos de Servicios Destacadas
        $video = $this->db->where('lang', $lang_admin)->get('home_video_info')->row();
        // consultamos los datos de Historia en numeros
        $numbers = $this->db->where('lang', $lang_admin)->order_by('position', 'ASC')->get('history_numbers')->result();
        
        // Consultamos los datos del slider de Clientes
        $recipes = $this->db->get('home_recipes')->result();

        //Consultamos los datos del texto informativo
        $destacado = $this->db->where('lang', $lang_admin)->get('home_outstanding')->row();

        // agregamos los atributos al template
        $this->template
                ->append_js('module::admin/ajax.js')
                ->set('banner', $banner)
                ->set('linkintro', $linkintro)
                ->set('text_info', $text_info)
                ->set('people', $people)
                ->set('customers', $customers)
                ->set('recipes', $recipes)
                ->set('numbers', $numbers)
                ->set('video', $video)
                ->set('destacado', $destacado)
                ->set('ordering', $ordering)
                ->set('lang_admin', $lang_admin)
                ->build('admin/index');
    }

    /*
     * Banner
     */

    public function create_banner($lang_admin = 'es') {
        // colocamos el titulo
        $titulo = 'Crear';
        // mandamos los datos al template
        $this->template
                ->set('titulo', $titulo)
                ->set('lang_admin', $lang_admin)
                ->build('admin/create_banner_back');
    }

    public function store_banner($lang_admin = 'es') {

        $this->form_validation->set_rules('image', 'Imagen', 'trim');

        if ($this->form_validation->run() == TRUE) {

            unset($_POST['btnAction']); // sacamos del array el valor del submit
            // cargamos los datos del formulario en una variable
            $data = $_POST;
            $data['lang'] = $lang_admin;

            // configuracion de la libreria upload
            $config['upload_path'] = './' . UPLOAD_PATH . '/home_banner';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2050;
            $config['encrypt_name'] = true;

            // cargamos la libreria upload
            $this->load->library('upload', $config);

            // imagen uno
            $img = $_FILES['image']['name'];

            // si la imagen no esta vacía la guardamos, y la guardamos en el array data
            if (!empty($img)) {
                if ($this->upload->do_upload('image')) {
                    $datos = array('upload_data' => $this->upload->data());
                    $path = UPLOAD_PATH . 'home_banner/' . $datos['upload_data']['file_name'];
                    $img = array('image' => $path);
                    $data = array_merge($data, $img);
                } else {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect('admin/home/index/' . $lang_admin);
                }
            }

            // insertamos en la base de datos
            if ($this->db->insert('home_banner', $data)) {
                $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
                redirect('admin/home/index/' . $lang_admin);
            } else {
                $this->session->set_flashdata('success', lang('home:error_message'));
                return $this->nuevo_destacado();
            }
        } else {
            if (validation_errors() == "") {
                $this->session->set_flashdata('error', validation_errors());
            }
            redirect('admin/home/#page-banner');
        }
    }

    public function edit_banner($idItem = null, $lang_admin = 'es') {
        // colocamos el titulo
        $titulo = 'Editar';
        // cargamos los datos
        $banner = $this->db->where('id', $idItem)->get('home_banner')->row();

        // mandamos los datos al template
        $this->template
                ->set('banner', $banner)
                ->set('titulo', $titulo)
                ->set('lang_admin', $lang_admin)
                ->build('admin/edit_banner_back');
    }

    public function update_banner($idItem = null, $lang_admin = 'es') {

        $this->form_validation->set_rules('image', 'Imagen', 'trim');

        if ($this->form_validation->run() !== TRUE) {  // abrimos el formulario de edicion
            if (validation_errors() == "") {
                $this->session->set_flashdata('error', validation_errors());
            }
        } else { // si el formulario ha sido enviado con éxito se procede
            unset($_POST['btnAction']); // sacamos del array el valor del submit
            // cargamos los datos del formulario en una variable
            $data = $_POST;
            $data['lang'] = $lang_admin;
            // configuracion de la libreria upload
            $config['upload_path'] = './' . UPLOAD_PATH . '/home_banner';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2050;
            $config['encrypt_name'] = true;

            // cargamos la libreria upload
            $this->load->library('upload', $config);

            // imagen uno
            $img = $_FILES['image']['name'];

            // si la imagen no esta vacía la guardamos, y la guardamos en el array data
            if (!empty($img)) {
                if ($this->upload->do_upload('image')) {
                    $datos = array('upload_data' => $this->upload->data());
                    $path = UPLOAD_PATH . 'home_banner/' . $datos['upload_data']['file_name'];
                    $img = array('image' => $path);
                    $data = array_merge($data, $img);
                    $obj = $this->db->where('id', $idItem)->get('home_banner')->row();
                    @unlink($obj->image);
                } else {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect('admin/home/index/' . $lang_admin);
                }
            }

            // actualizamos los datos
            if ($this->db->where('id', $idItem)->update('home_banner', $data)) {
                $this->session->set_flashdata('success', 'Los registros se actualizarón con éxito.');
                redirect('admin/home/index/' . $lang_admin);
            } else {
                $this->session->set_flashdata('success', lang('home:error_message'));
                redirect('admin/home/edit_banner/' . $idItem . '/' . $lang_admin);
            }
        }
    }

    public function delete_banner($id = null) {
        // si no existe el id volvemos al index
        $id or redirect('admin/home/');

        // guardamos los datos del banner
        $obj = $this->db->where('id', $id)->get($this->db->dbprefix . 'home_banner')->row();
        // borramos el registro
        if ($this->db->where('id', $id)->delete('home_banner')) {
            // borramos la imaen
            @unlink($obj->image);
            $this->session->set_flashdata('success', 'El registro se elimino con éxito.');
        } else {
            $this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
        }
        redirect('admin/home/');
    }

    /*
     * Destacado
     */

    public function edit_outstanding($lang_admin = 'es') {
        // reglas de validación
        $this->form_validation->set_rules('title', 'Titulo', 'trim');
        $this->form_validation->set_rules('text', 'Text', 'trim');
        $this->form_validation->set_rules('subtitle', 'Subtitulo', 'trim');

        // abrimos el formulario de edicion
        if ($this->form_validation->run() !== TRUE) {
            if (validation_errors() == "") {
                $this->session->set_flashdata('error', validation_errors());
            }
            redirect('admin/home/#page-destacado');
        } else { // si el formulario ha sido enviado con éxito se procede
            unset($_POST['btnAction']); // borramos el dato del boton submit
            $_POST['lang'] = $lang_admin; // guardamos el dato del idioma
            $data = $_POST; // guardamos los datos del post en una variable
            // configuración de la libreria upload
            $config['upload_path'] = './' . UPLOAD_PATH . '/home_outstanding';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '10000000';
            $config['encrypt_name'] = true;

            // cargamos la libreria uploads
            $this->load->library('upload', $config);

            // imagen uno
            $img = $_FILES['image']['name'];

            // si la imagen no esta vacía, la guardamos y la agregamos al array
            if (!empty($img)) {
                if ($this->upload->do_upload('image')) {
                    $datos = array('upload_data' => $this->upload->data());
                    $path = UPLOAD_PATH . 'home_outstanding/' . $datos['upload_data']['file_name'];
                    $img = array('image' => $path);
                    $data = array_merge($data, $img);
                    $obj = $this->db->where('id', $idItem)->get('home_outstanding')->row();
                    @unlink($obj->imagen);
                } else {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    header('Location: ' . $_SERVER['REQUEST_URI']);
                }
            }

            // actualizamos el registro
            if ($this->db->where('lang', $lang_admin)->update('home_outstanding', $data)) {
                $this->session->set_flashdata('success', 'Los registros se actualizarón con éxito.');
            } else {
                $this->session->set_flashdata('success', lang('home:error_message'));
            }
            redirect('admin/home/index/' . $lang_admin . '/#page-destacado');
        }
    }
    
    /*
     * Beneficios
     */

    public function edit_link_info($lang_admin = 'es') {
        // reglas de validación
        $this->form_validation->set_rules('title', 'Titulo', 'trim');

        // abrimos el formulario de edicion
        if ($this->form_validation->run() !== TRUE) {
            if (validation_errors() == "") {
                $this->session->set_flashdata('error', validation_errors());
            }
            redirect('admin/home/index/' . $lang_admin . '/#page-people');
        } else { // si el formulario ha sido enviado con éxito se procede
            unset($_POST['btnAction']); // borramos el dato del boton submit
            $_POST['lang'] = $lang_admin; // guardamos el dato del idioma
            $data = $_POST; // guardamos los datos del post en una variable
            // configuración de la libreria upload
            $config['upload_path'] = './' . UPLOAD_PATH . '/home_outstanding';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '10000000';
            $config['encrypt_name'] = true;

            // cargamos la libreria uploads
            $this->load->library('upload', $config);

            // imagen uno
            $img = $_FILES['image']['name'];

            // si la imagen no esta vacía, la guardamos y la agregamos al array
            if (!empty($img)) {
                if ($this->upload->do_upload('image')) {
                    $datos = array('upload_data' => $this->upload->data());
                    $path = UPLOAD_PATH . 'home_outstanding/' . $datos['upload_data']['file_name'];
                    $img = array('image' => $path);
                    $data = array_merge($data, $img);
                    $obj = $this->db->where('lang', $lang_admin)->get('home_links_intro')->row();
                    @unlink($obj->image);
                } else {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect('admin/home/index/' . $lang_admin . '/#page-people');
                }
            }

            // actualizamos el registro
            if ($this->db->where('lang', $lang_admin)->update('home_links_intro', $data)) {
                $this->session->set_flashdata('success', 'Los registros se actualizarón con éxito.');
                redirect('admin/home/index/' . $lang_admin . '/#page-people');
            } else {
                $this->session->set_flashdata('success', lang('home:error_message'));
                redirect('admin/home/index/' . $lang_admin . '/#page-people');
            }
            redirect('admin/home/index/' . $lang_admin . '/#page-people');
        }
    }

    public function edit_people($idItem = null) {
        // reglas de validación
        $this->form_validation->set_rules('title', 'Nombre', 'trim');

        // abrimos el formulario de edicion
        if ($this->form_validation->run() !== TRUE) {
            if (validation_errors() == "") {
                $this->session->set_flashdata('error', validation_errors());
            }
            if (!empty($idItem)) {  // si se envia un dato por la URL se hace lo siguiente (Edita)
                // colocamos el titulo
                $titulo = 'Editar';
                // cargamos los datos
                $dataForm = $this->db->where('id', $idItem)->get('people')->row();
                // mandamos los datos al template
                $this->template
                        ->set('dataForm', $dataForm)
                        ->set('titulo', $titulo)
                        ->build('admin/edit_people_back');
            } else {
                // colocamos el titulo
                $titulo = 'Crear';
                // mandamos los datos al template
                $this->template
                        ->set('titulo', $titulo)
                        ->build('admin/edit_people_back');
            }
        } else { // si el formulario ha sido enviado con éxito se procede
            $action = $_POST['btnAction'];  // guardamos el valor del submit
            unset($_POST['btnAction']); // borramos el valor del submit
            $data = $_POST; // guardamos los datos que se enviaron por el post
            if (!empty($idItem)) {  // si se envia un dato por la URL se hace lo siguiente (Edita)
                // configuración de la libreria upload
                $config['upload_path'] = './' . UPLOAD_PATH . '/people';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = 2050;
                $config['encrypt_name'] = true;

                // cargamos la libreria uploads
                $this->load->library('upload', $config);

                // imagen uno
                $img = $_FILES['image']['name'];

                // si la imagen no esta vacía, la guardamos y la agregamos al array
                if (!empty($img)) {
                    if ($this->upload->do_upload('image')) {
                        $datos = array('upload_data' => $this->upload->data());
                        $path = UPLOAD_PATH . 'people/' . $datos['upload_data']['file_name'];
                        $img = array('image' => $path);
                        $data = array_merge($data, $img);
                        $obj = $this->db->where('id', $idItem)->get('people')->row();
                        @unlink($obj->imagen);
                    } else {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        header('Location: ' . $_SERVER['REQUEST_URI']);
                    }
                }

                // actualizamos el registro
                if ($this->db->where('id', $idItem)->update('people', $data)) {
                    $this->session->set_flashdata('success', 'Los registros se actualizarón con éxito.');
                    redirect('admin/home/#page-people');
                } else {
                    $this->session->set_flashdata('success', lang('home:error_message'));
                    header('Location: ' . $_SERVER['REQUEST_URI']);
                }
            } else {
                // configuración de la libreria uploads
                $config['upload_path'] = './' . UPLOAD_PATH . '/people';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = 2050;
                $config['encrypt_name'] = true;

                // cargamos la libreria uploads
                $this->load->library('upload', $config);

                // imagen uno
                $img = $_FILES['image']['name'];

                // si la imagen no esta vacía la guardamos y la agregamos al array
                if (!empty($img)) {
                    if ($this->upload->do_upload('image')) {
                        $datos = array('upload_data' => $this->upload->data());
                        $path = UPLOAD_PATH . 'people/' . $datos['upload_data']['file_name'];
                        $img = array('image' => $path);
                        $data = array_merge($data, $img);
                    } else {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        header('Location: ' . $_SERVER['REQUEST_URI']);
                    }
                }

                // insertamos el registro
                if ($this->db->insert('people', $data)) {
                    $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
                    redirect('admin/home/#page-people');
                } else {
                    $this->session->set_flashdata('error', lang('home:error_message'));
                    header('Location: ' . $_SERVER['REQUEST_URI']);
                }
            }
        }
    }

    public function delete_people($id = null) {
        // si no se recibe el id lo mandamos al index
        $id or redirect('admin/home/#page-people');

        // consultamos el registro
        $obj = $this->db->where('id', $id)->get($this->db->dbprefix . 'people')->row();
        // borramos el registro
        if ($this->db->where('id', $id)->delete('people')) {
            // borramos la imagen
            @unlink($obj->image);
            $this->session->set_flashdata('success', 'El registro se elimino con éxito.');
        } else {
            $this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
        }
        redirect('admin/home/#page-people');
    }
    
    /*
     * Sabores
     */
    
    public function edit_video_info($lang_admin = 'es') {
        // reglas de validación
        $this->form_validation->set_rules('title', 'Titulo', 'trim');
        $this->form_validation->set_rules('text', 'Text', 'trim');

        // abrimos el formulario de edicion
        if ($this->form_validation->run() !== TRUE) {
            if (validation_errors() == "") {
                $this->session->set_flashdata('error', validation_errors());
            }
            redirect('admin/home/#page-video-info');
        } else { // si el formulario ha sido enviado con éxito se procede
            unset($_POST['btnAction']); // borramos el dato del boton submit
            $_POST['lang'] = $lang_admin; // guardamos el dato del idioma
            $data = $_POST; // guardamos los datos del post en una variable
//            echo "<pre>";var_dump($data);die;
            // actualizamos el registro
            if ($this->db->where('lang', $lang_admin)->update('home_video_info', $data)) {
                $this->session->set_flashdata('success', 'Los registros se actualizarón con éxito.');
            } else {
                $this->session->set_flashdata('success', lang('home:error_message'));
            }
            redirect('admin/home/index/' . $lang_admin . '/#page-video-info');
        }
    }
    
    public function edit_customers($idItem = null) {
        // reglas de validación
        $this->form_validation->set_rules('name', 'Nombre', 'trim');

        // abrimos el formulario de edicion
        if ($this->form_validation->run() !== TRUE) {
            if (validation_errors() == "") {
                $this->session->set_flashdata('error', validation_errors());
            }
            if (!empty($idItem)) {  // si se envia un dato por la URL se hace lo siguiente (Edita)
                // colocamos el titulo
                $titulo = 'Editar';
                // cargamos los datos
                $dataForm = $this->db->where('id', $idItem)->get('home_customers')->row();
                // mandamos los datos al template
                $this->template
                        ->set('dataForm', $dataForm)
                        ->set('titulo', $titulo)
                        ->build('admin/edit_customers_back');
            } else {
                // colocamos el titulo
                $titulo = 'Crear';
                // mandamos los datos al template
                $this->template
                        ->set('titulo', $titulo)
                        ->build('admin/edit_customers_back');
            }
        } else { // si el formulario ha sido enviado con éxito se procede
            $action = $_POST['btnAction'];  // guardamos el valor del submit
            unset($_POST['btnAction']); // borramos el valor del submit
            $data = $_POST; // guardamos los datos que se enviaron por el post
            if (!empty($idItem)) {  // si se envia un dato por la URL se hace lo siguiente (Edita)
                // configuración de la libreria upload
                $config['upload_path'] = './' . UPLOAD_PATH . '/home_customers';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = 2050;
                $config['encrypt_name'] = true;

                // cargamos la libreria uploads
                $this->load->library('upload', $config);

                // imagen uno
                $img = $_FILES['image']['name'];

                // si la imagen no esta vacía, la guardamos y la agregamos al array
                if (!empty($img)) {
                    if ($this->upload->do_upload('image')) {
                        $datos = array('upload_data' => $this->upload->data());
                        $path = UPLOAD_PATH . 'home_customers/' . $datos['upload_data']['file_name'];
                        $img = array('image' => $path);
                        $data = array_merge($data, $img);
                        $obj = $this->db->where('id', $idItem)->get('home_customers')->row();
                        @unlink($obj->image);
                    } else {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        header('Location: ' . $_SERVER['REQUEST_URI']);
                    }
                }
                
                // imagen dos
                $img2 = $_FILES['image2']['name'];

                // si la imagen no esta vacía, la guardamos y la agregamos al array
                if (!empty($img2)) {
                    if ($this->upload->do_upload('image2')) {
                        $datos = array('upload_data' => $this->upload->data());
                        $path = UPLOAD_PATH . 'home_customers/' . $datos['upload_data']['file_name'];
                        $img2 = array('image2' => $path);
                        $data = array_merge($data, $img2);
                        $obj = $this->db->where('id', $idItem)->get('home_customers')->row();
                        @unlink($obj->image2);
                    } else {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        header('Location: ' . $_SERVER['REQUEST_URI']);
                    }
                }

                // actualizamos el registro
                if ($this->db->where('id', $idItem)->update('home_customers', $data)) {
                    $this->session->set_flashdata('success', 'Los registros se actualizarón con éxito.');
                    redirect('admin/home/#page-banner-customers');
                } else {
                    $this->session->set_flashdata('success', lang('home:error_message'));
                    header('Location: ' . $_SERVER['REQUEST_URI']);
                }
            } else {
                // configuración de la libreria uploads
                $config['upload_path'] = './' . UPLOAD_PATH . '/home_customers';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = 2050;
                $config['encrypt_name'] = true;

                // cargamos la libreria uploads
                $this->load->library('upload', $config);

                // imagen uno
                $img = $_FILES['image']['name'];

                // si la imagen no esta vacía la guardamos y la agregamos al array
                if (!empty($img)) {
                    if ($this->upload->do_upload('image')) {
                        $datos = array('upload_data' => $this->upload->data());
                        $path = UPLOAD_PATH . 'home_customers/' . $datos['upload_data']['file_name'];
                        $img = array('image' => $path);
                        $data = array_merge($data, $img);
                    } else {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        header('Location: ' . $_SERVER['REQUEST_URI']);
                    }
                }

                // insertamos el registro
                if ($this->db->insert('home_customers', $data)) {
                    $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
                    redirect('admin/home/#page-banner-customers');
                } else {
                    $this->session->set_flashdata('error', lang('home:error_message'));
                    header('Location: ' . $_SERVER['REQUEST_URI']);
                }
            }
        }
    }

    public function delete_customers($id = null) {
        // si no se recibe el id lo mandamos al index
        $id or redirect('admin/home/#page-banner-customers');

        // consultamos el registro
        $obj = $this->db->where('id', $id)->get($this->db->dbprefix . 'home_customers')->row();
        // borramos el registro
        if ($this->db->where('id', $id)->delete('home_customers')) {
            // borramos la imagen
            @unlink($obj->image);
            $this->session->set_flashdata('success', 'El registro se elimino con éxito.');
        } else {
            $this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
        }
        redirect('admin/home/#page-banner-customers');
    }
    
    /*
     * Recetas
     */
    
    public function edit_recipes($idItem = null) {
        // reglas de validación
        $this->form_validation->set_rules('name', 'Nombre', 'trim');

        // abrimos el formulario de edicion
        if ($this->form_validation->run() !== TRUE) {
            if (validation_errors() == "") {
                $this->session->set_flashdata('error', validation_errors());
            }
            if (!empty($idItem)) {  // si se envia un dato por la URL se hace lo siguiente (Edita)
                // colocamos el titulo
                $titulo = 'Editar';
                // cargamos los datos
                $dataForm = $this->db->where('id', $idItem)->get('home_recipes')->row();
                if (!empty($dataForm->video)) {
                    if ($this->_is_youtube($dataForm->video)) {
                        $videoJson = json_decode(file_get_contents('http://www.youtube.com/oembed?url=' . $dataForm->video . '&format=json'));
                        $dataForm->image = $videoJson->thumbnail_url;
                        $dataForm->text = substr($videoJson->description, 0, 170);
                        $dataForm->link = $dataForm->video;
                        preg_match('/src="([^"]+)"/', $videoJson->html, $match);
                        $url = $match[1];
                        $dataForm->content = $url;
                    }
                    if ($this->_is_vimeo($dataForm->video)) {
                        $videoJson = json_decode(file_get_contents('http://vimeo.com/api/oembed.json?url=' . $dataForm->video . ''));
                        $dataForm->image = $videoJson->thumbnail_url;
                        $dataForm->text = substr($videoJson->description, 0, 160);
                        $dataForm->link = $dataForm->video;
                        $dataForm->content = 'https://player.vimeo.com/video/' . $videoJson->video_id;
                    }
                }
                // mandamos los datos al template
                $this->template
                        
                        ->set('dataForm', $dataForm)
                        ->set('titulo', $titulo)
                        ->build('admin/edit_recipes_back');
            } else {
                // colocamos el titulo
                $titulo = 'Crear';
                // mandamos los datos al template
                $this->template
                        ->set('titulo', $titulo)
                        ->build('admin/edit_recipes_back');
            }
        } else { // si el formulario ha sido enviado con éxito se procede
            $action = $_POST['btnAction'];  // guardamos el valor del submit
            unset($_POST['btnAction']); // borramos el valor del submit
            $post = (object) $this->input->post();
            
            $data = array(
                'title' => $post->title,
                'video' => $post->video,
                'type' => $post->type,
                'ingredients' => html_entity_decode($post->ingredients),
                'preparation' => html_entity_decode($post->preparation)
            );
//            echo "<pre>";var_dump($data);die;
            if (!empty($idItem)) {  // si se envia un dato por la URL se hace lo siguiente (Edita)
                // configuración de la libreria upload
                $config['upload_path'] = './' . UPLOAD_PATH . '/home_customers';
                $config['allowed_types'] = 'mp4';
                $config['encrypt_name'] = true;

                // cargamos la libreria uploads
                $this->load->library('upload', $config);

                // imagen uno
                $img = $_FILES['path']['name'];

                // si la imagen no esta vacía, la guardamos y la agregamos al array
                if (!empty($img)) {
                    if ($this->upload->do_upload('path')) {
                        $datos = array('upload_data' => $this->upload->data());
                        $path = UPLOAD_PATH . 'home_customers/' . $datos['upload_data']['file_name'];
                        $img = array('path' => $path);
                        $data = array_merge($data, $img);
                        $obj = $this->db->where('id', $idItem)->get('home_recipes')->row();
                        @unlink($obj->path);
                    } else {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        header('Location: ' . $_SERVER['REQUEST_URI']);
                    }
                }
                
                // actualizamos el registro
                if ($this->db->where('id', $idItem)->update('home_recipes', $data)) {
                    $this->session->set_flashdata('success', 'Los registros se actualizarón con éxito.');
                    redirect('admin/home/index/#page-recipes');
                } else {
                    $this->session->set_flashdata('success', lang('home:error_message'));
                    header('Location: ' . $_SERVER['REQUEST_URI']);
                }
            } else {
                // configuración de la libreria uploads
                $config['upload_path'] = './' . UPLOAD_PATH . '/home_customers';
                $config['allowed_types'] = 'mp4';
                $config['encrypt_name'] = true;

                // cargamos la libreria uploads
                $this->load->library('upload', $config);

                // imagen uno
                $img = $_FILES['path']['name'];

                // si la imagen no esta vacía la guardamos y la agregamos al array
                if (!empty($img)) {
                    if ($this->upload->do_upload('path')) {
                        $datos = array('upload_data' => $this->upload->data());
                        $path = UPLOAD_PATH . 'home_customers/' . $datos['upload_data']['file_name'];
                        $img = array('path' => $path);
                        $data = array_merge($data, $img);
                    } else {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        header('Location: ' . $_SERVER['REQUEST_URI']);
                    }
                }

                // insertamos el registro
                if ($this->db->insert('home_recipes', $data)) {
                    $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
                    redirect('admin/home/index/#page-recipes');
                } else {
                    $this->session->set_flashdata('error', lang('home:error_message'));
                    header('Location: ' . $_SERVER['REQUEST_URI']);
                }
            }
        }
    }

    public function delete_recipes($id = null) {
        // si no se recibe el id lo mandamos al index
        $id or redirect('admin/home/#page-recipes');

        // consultamos el registro
        $obj = $this->db->where('id', $id)->get($this->db->dbprefix . 'home_recipes')->row();
        // borramos el registro
        if ($this->db->where('id', $id)->delete('home_recipes')) {
            // borramos la imagen
            @unlink($obj->image);
            $this->session->set_flashdata('success', 'El registro se elimino con éxito.');
        } else {
            $this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
        }
        redirect('admin/home/#page-recipes');
    }
    
    /*
     * Donde Comprar
     */
    
    public function edit_numbers_info($lang_admin = 'es') {
        // reglas de validación
        $this->form_validation->set_rules('title2', 'Titulo', 'trim');

        // abrimos el formulario de edicion
        if ($this->form_validation->run() !== TRUE) {
            if (validation_errors() == "") {
                $this->session->set_flashdata('error', validation_errors());
            }
            redirect('admin/home/#page-numbers');
        } else { // si el formulario ha sido enviado con éxito se procede
            unset($_POST['btnAction']); // borramos el dato del boton submit
            $_POST['lang'] = $lang_admin; // guardamos el dato del idioma
            $data = $_POST; // guardamos los datos del post en una variable
            
            // actualizamos el registro
            if ($this->db->where('lang', $lang_admin)->update('home_links_intro', $data)) {
                $this->session->set_flashdata('success', 'Los registros se actualizarón con éxito.');
            } else {
                $this->session->set_flashdata('success', lang('home:error_message'));
            }
            redirect('admin/home/index/' . $lang_admin . '/#page-numbers');
        }
    }
    
    public function delete_numbers($id = null) {
        // si no se recibe el id lo mandamos al index
        $id or redirect('admin/home/#page-numbers');

        // consultamos el registro
        $obj = $this->db->where('id', $id)->get($this->db->dbprefix . 'history_numbers')->row();
        // borramos el registro
        if ($this->db->where('id', $id)->delete('history_numbers')) {
            // borramos la imagen
            @unlink($obj->image);
            $this->session->set_flashdata('success', 'El registro se elimino con éxito.');
        } else {
            $this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
        }
        redirect('admin/home/#page-numbers');
    }

    public function edit_numbers($idItem = null, $lang_admin = 'es') {
        // Reglas de validación
        $this->form_validation->set_rules('title', 'Título', 'trim');
        $this->form_validation->set_rules('text', 'Texto', 'trim');
        $this->form_validation->set_rules('link', 'Link', 'valid_url|trim');

        if ($this->form_validation->run() !== TRUE) {  // abrimos el formulario de edicion
            if (validation_errors() == "") {
                $this->session->set_flashdata('error', validation_errors());
            }
            if (!empty($idItem)) {  // si se envia un dato por la URL se hace lo siguiente (Edita)
                // colocamos el titulo dependiendo del tipo
                $titulo = 'Editar';
                // cargamos los datos
                $outstanding = $this->db->where('id', $idItem)->get('history_numbers')->row();

                // mandamos los datos al template
                $this->template
                        ->set('outstanding', $outstanding)
                        ->set('titulo', $titulo)
                        ->set('lang_admin', $lang_admin)
                        ->build('admin/edit_numbers_back');
            } else {
                // colocamos el titulo dependiendo del tipo
                $titulo = 'Crear';

                // mandamos los datos al template
                $this->template
                        ->set('titulo', $titulo)
                        ->set('lang_admin', $lang_admin)
                        ->build('admin/edit_numbers_back');
            }
        } else { // si el formulario ha sido enviado con éxito se procede
            unset($_POST['btnAction']); // sacamos del array el valor del submit
            // cargamos los datos del formulario en una variable
            $data = $_POST;
            $data['lang'] = $lang_admin;
            if (!empty($idItem)) {  // si se envia un dato por la URL se hace lo siguiente (Edita)
                
                // actualizamos los datos
                if ($this->db->where('id', $idItem)->update('history_numbers', $data)) {
                    $this->session->set_flashdata('success', 'Los registros se actualizarón con éxito.');
                    redirect('admin/home/#page-numbers');
                } else {
                    $this->session->set_flashdata('success', lang('home:error_message'));
                    return $this->editar_destacado();
                }
            } else {
                
                // insertamos los datos
                if ($this->db->insert('history_numbers', $data)) {
                    $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
                    redirect('admin/home/#page-numbers');
                } else {
                    $this->session->set_flashdata('success', lang('home:error_message'));
                    return $this->nuevo_destacado();
                }
            }
        }
    }
    
    public function orden_numbers() {
        // inicializamos las variables
        $statusJson = '';
        $msgJson = '';
        $datosArray = $_POST['subCatArray'];  // tomamos los datos del post y se los damos al data

        $datosArray = array_unique($datosArray);  // quitamos los repetidos
        $datosArray = array_values($datosArray);  // ordenamos el array de 0 a n
        // ponemos el orden de las categorias
        $i = 1;
        foreach ($datosArray as $fila => $idRegistro) {
            $data = array(
                'position' => $i,
            );
            // actualizamos el registro
            if ($this->db->where('id', $idRegistro)->update('history_numbers', $data)) {
                if ($statusJson != "error") {
                    $statusJson = "";
                    $msgJson = "El campo se ha cambiado con Ã©xito.";
                }
            } else {
                $statusJson = "error";
                $msgJson = "OcurriÃ³ un error. Actualizando las posiciones";
            }
            $i++;
        }

        echo json_encode(array('status' => $statusJson, 'msg' => $msgJson));
    }
    
    //Lugares 
    
    public function places($idcity = null){
        $city = $this->db->where('id', $idcity)->get('history_numbers')->row();
        $places = $this->db->where('id_city', $idcity)->get('places')->result();
        $this->template
                ->set('city', $city)
                ->set('places', $places)
                ->build('admin/places');
    }
    
    public function create_place($id_city = null, $lang_admin = 'es') {
        // colocamos el titulo
        $titulo = 'Crear';
        // mandamos los datos al template
        $this->template
                ->set('titulo', $titulo)
                ->set('city', $id_city)
                ->set('lang_admin', $lang_admin)
                ->build('admin/create_place');
    }

    public function store_place($idcity = null, $lang_admin = 'es') {

        $this->form_validation->set_rules('image', 'Imagen', 'trim');

        if ($this->form_validation->run() == TRUE) {

            unset($_POST['btnAction']); // sacamos del array el valor del submit
            // cargamos los datos del formulario en una variable
            $data = $_POST;
            $data['lang'] = $lang_admin;

            // insertamos en la base de datos
            if ($this->db->insert('places', $data)) {
                $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
                redirect('admin/home/places/'.$idcity);
            } else {
                $this->session->set_flashdata('success', lang('home:error_message'));
                redirect('admin/home/places/'.$idcity);
            }
        } else {
            if (validation_errors() == "") {
                $this->session->set_flashdata('error', validation_errors());
            }
            redirect('admin/home/places/'.$idcity);
        }
    }

    public function edit_place($idItem = null, $lang_admin = 'es') {
        // colocamos el titulo
        $titulo = 'Editar';
        // cargamos los datos
        $banner = $this->db->where('id', $idItem)->get('places')->row();

        // mandamos los datos al template
        $this->template
                ->set('banner', $banner)
                ->set('titulo', $titulo)
                ->set('lang_admin', $lang_admin)
                ->build('admin/edit_place');
    }

    public function update_place($idItem = null, $city = null) {

        $this->form_validation->set_rules('image', 'Imagen', 'trim');

        if ($this->form_validation->run() !== TRUE) {  // abrimos el formulario de edicion
            if (validation_errors() == "") {
                $this->session->set_flashdata('error', validation_errors());
            }
        } else { // si el formulario ha sido enviado con éxito se procede
            unset($_POST['btnAction']); // sacamos del array el valor del submit
            // cargamos los datos del formulario en una variable
            $data = $_POST;
            $data['lang'] = $lang_admin;
            
            // actualizamos los datos
            if ($this->db->where('id', $idItem)->update('places', $data)) {
                $this->session->set_flashdata('success', 'Los registros se actualizarón con éxito.');
                redirect('admin/home/places/' . $city);
            } else {
                $this->session->set_flashdata('success', lang('home:error_message'));
                redirect('admin/home/places/' . $city);
            }
        }
    }

    public function delete_place($id = null, $city = null) {
        // si no existe el id volvemos al index
        $id or redirect('admin/home/places/'. $city);

        // borramos el registro
        if ($this->db->where('id', $id)->delete('places')) {
            $this->session->set_flashdata('success', 'El registro se elimino con éxito.');
        } else {
            $this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
        }
        redirect('admin/home/places/'. $city);
    }

    //contacto
    
    public function export_contact_mail($status = 1) {
        $this->load->library(array('to_excel'));
        $headers = array();
        $namefile = "Mensajes";
        $datosSql = $this->db
                ->select('ce.name AS Nombre, ce.email AS Correo, ce.phone AS Telefono, ce.city AS Ciudad, ce.message AS Mensaje')
                ->from('contact_us_emails AS ce')
                ->get();

        $i = 0;

        foreach ($datosSql->result_array() as $row => $value) {
            $i = 0;
            foreach ($value as $row2 => $value2) {
                array_push($headers, $row2);
                $i++;
            }
            break;
        }
        $arrayExcel = $datosSql->result();
        array_unshift($arrayExcel, $headers);
        $this->to_excel($datosSql, $namefile);
    }

    // pasar un query sql de codeigniter a excel
    function to_excel($datosSql, $filename = 'exceloutput') {
        $encabezadoExcel = '';  // just creating the var for field headers to append to below
        $datosExcel = '';  // just creating the var for field data to append to below	     

        $obj = & get_instance();

        if ($datosSql->num_rows() == 0) {
            echo '<p>La tabla no tiene datos.</p>';
        } else {
            foreach ($datosSql->result_array() as $row => $value) {
                $encabezadoExcel = '';
                foreach ($value as $row2 => $value2) {
                    $encabezadoExcel .= $row2 . "\t";
                }
            }

            foreach ($datosSql->result() as $row) {
                $lineaExcel = '';
                foreach ($row as $value) {
                    if ((!isset($value)) OR ( $value == "")) {
                        $value = "\t";
                    } else {
                        $value = str_replace('"', '""', $value);
                        $value = '"' . $value . '"' . "\t";
                    }
                    $lineaExcel .= $value;
                }
                $datosExcel .= trim($lineaExcel) . "\n";
            }

            $datosExcel = str_replace("\r", "", $datosExcel);

            // pasar los datos de utf-8 a ansi
            $datosExcel = iconv('UTF-8', 'ISO-8859-1', $datosExcel);
            $encabezadoExcel = iconv('UTF-8', 'ISO-8859-1', $encabezadoExcel);
            header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");  // for Excel 2007
            header("Content-Disposition: attachment; filename=$filename.xls");
            echo "$encabezadoExcel\n$datosExcel";
        }
    }
    
    public function delete_email_dashboard($id = null) {
        $id or redirect('admin/home/');
        if ($this->db->where('id', $id)->delete('contact_us_emails')) {
            $this->session->set_flashdata('success', 'El registro se elimino con éxito.');
        } else {
            $this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
        }
        redirect('admin');
    }
    
    public function _is_youtube($url) {
        return (preg_match('/youtu\.be/i', $url) || preg_match('/youtube\.com\/watch/i', $url));
    }

    public function _is_vimeo($url) {
        return (preg_match('/vimeo\.com/i', $url));
    }

}
