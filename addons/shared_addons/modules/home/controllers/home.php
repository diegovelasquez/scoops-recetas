<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @author 	Luis Fernando Salazar Buitrago
 * @author  Brayan Acebo
 * @package 	PyroCMS
 * @subpackage 	Home Module
 * @category 	Modulos
 * @license 	Apache License v2.0
 */
class Home extends Public_Controller {

    public function __construct() {
        parent::__construct();
        /* idioma */
        $this->lang_code = $this->session->userdata('lang_code');
        if (empty($this->lang_code)) {
            $_SESSION['lang_code'] = 'es';
            $this->session->set_userdata(array('lang_code' => 'es'));
            $this->lang_code = $this->session->userdata('lang_code');
        }
        $this->template
                ->append_js('module::jquery_validate.js')
                ->append_js('module::formvalidate.js');
    }

    // -----------------------------------------------------------------

    public function index() {

        $lang = $this->lang_code;

        // consultamos los datos del Banner
        $banner = $this->db->where('lang', $lang)->get('home_banner')->result();
        //Consultamos los datos del destacado
        $destacado = $this->db->where('lang', $lang)->get('home_outstanding')->row();
        //Consultamos los datos del texto testimonios
        $linkintro = $this->db->where('lang', $lang)->get('home_links_intro')->row();
        $text_info = $this->db->where('id', 1)->get('home_text_info')->row();
        // Consultamos los datos del slider de Clientes
        $customers = $this->db->get('home_customers')->result();
        // Consultamos los datos del slider de Clientes
        $people = $this->db->where('lang', $lang)->get('people')->result();
        $i = 1;
        foreach($people as $pe){
            if($i%2 == 0){
                $pe->par = '';
            }else{
                $pe->par = '2';
            }
            $i++;
        }
        
        //contacto
        $contact = $this->db->where('lang', $lang)->order_by('position', 'ASC')->get('history_numbers')->result();
        foreach($contact as $co){
            $co->places = $this->db->where('id_city', $co->id)->get('places')->result();
        }
        // consultamos los datos de videos
        $video = $this->db->where('lang', $lang)->get('home_video_info')->row();
        
        $recipes = $this->db->get('home_recipes')->result();
        foreach($recipes as $recipe){
            if($recipe->type == 1){
                if (!empty($recipe->video)) {
                    if ($this->_is_youtube($recipe->video)) {
                        $videoJson = json_decode(file_get_contents('http://www.youtube.com/oembed?url=' . $recipe->video . '&format=json'));
                        $recipe->image = $videoJson->thumbnail_url;
                        $recipe->text = substr($videoJson->description, 0, 170);
                        $recipe->link = $recipe->video;
                        preg_match('/src="([^"]+)"/', $videoJson->html, $match);
                        $url = $match[1];
                        $recipe->content = $url;
                    }
                    if ($this->_is_vimeo($recipe->video)) {
                        $videoJson = json_decode(file_get_contents('http://vimeo.com/api/oembed.json?url=' . $recipe->video . ''));
                        $recipe->image = $videoJson->thumbnail_url;
                        $recipe->text = substr($videoJson->description, 0, 160);
                        $recipe->link = $recipe->video;
                        $recipe->content = 'https://player.vimeo.com/video/' . $videoJson->video_id;
                    }
                }
            }
        }
        
        $map = $this->db->where('lang', $lang)->get('google_maps')->row();
        // agregamos los atributos al template
        $this->template
                ->append_js('module::google_maps.js')
                ->set('banner', $banner)
                ->set('destacado', $destacado)
                ->set('linkintro', $linkintro)
                ->set('text_info', $text_info)
                ->set('customers', $customers)
                ->set('people', $people)
                ->set('video', $video)
                ->set('recipes', $recipes)
                ->set('contact', $contact)
                ->set('map', $map)
                ->set('lang', $lang)
                ->build('index');
    }
    
    public function _is_youtube($url) {
        return (preg_match('/youtu\.be/i', $url) || preg_match('/youtube\.com\/watch/i', $url));
    }

    public function _is_vimeo($url) {
        return (preg_match('/vimeo\.com/i', $url));
    }

    public function change_lenguage($lang) {
        $this->session->set_userdata(array('lang_code' => $lang));
        $_SESSION['lang_code'] = $lang;
        redirect($_SERVER['HTTP_REFERER']);
    }

}
