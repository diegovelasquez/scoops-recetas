<section class="item">
    <div class="content">
        <h2>Sabores</h2>
        <div class="tabs">
            <ul class="tab-menu">
                <li><a href="#page-principal"><span><?php echo $titulo; ?></span></a></li>
            </ul>

            <div class="inline-form" id="page-principal">
                <?php echo form_open_multipart(site_url('admin/home/edit_customers/' . (isset($dataForm) ? $dataForm->id : '0') . '/' . $lang_admin), 'class="crud"'); ?>
                <fieldset>
                    <ul>
                        <li>
                            <label for="name">Imagen
                                <small>
                                    - Imagen Permitidas gif | jpg | png | jpeg<br>
                                    - Todas las imagenes deben ser del mismo tamaño
                                </small>
                            </label>
                            <?php if (!empty($dataForm->image)): ?>
                                <div>
                                    <img src="<?php echo site_url($dataForm->image) ?>" width="298">
                                </div>
                            <?php endif; ?>
                            <div class="btn-false">
                                <div class="btn">Examinar</div>
                                <?php echo form_upload('image', set_value('image'), ' id="image"'); ?>
                            </div>
                        </li>
                        <li>
                            <label for="name">Tabla nutricional
                                <small>
                                    - Imagen Permitidas gif | jpg | png | jpeg<br>
                                    - Todas las imagenes deben ser del mismo tamaño
                                </small>
                            </label>
                            <?php if (!empty($dataForm->image2)): ?>
                                <div>
                                    <img src="<?php echo site_url($dataForm->image2) ?>" width="298">
                                </div>
                            <?php endif; ?>
                            <div class="btn-false">
                                <div class="btn">Examinar</div>
                                <?php echo form_upload('image2', set_value('image2'), ' id="image2"'); ?>
                            </div>
                        </li>
                        <li>
                            <label for="name">Nombre</label>
                            <div class="input"><?php echo form_input('name', (isset($dataForm->name)) ? $dataForm->name : set_value('name'), 'class="dev-input-title" style="width:100%"'); ?></div>
                        </li>
                        <li>
                            <label for="name">Descripción</label>
                            <div class="input">
                                <?php echo form_input('text', (isset($dataForm->text)) ? $dataForm->text : set_value('text'), 'class="dev-input-title" style="width:100%"'); ?>
                            </div>
                        </li>
                    </ul>
                    <?php
                    $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel')));
                    ?>
                </fieldset>
                <?php echo form_close(); ?>
            </div>

        </div>
    </div>
</section>