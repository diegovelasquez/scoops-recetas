<section class="item">
    <div class="content">
        <h2>Beneficios </h2>
        <div class="tabs">
            <ul class="tab-menu">
                <li><a href="#page-principal"><span><?php echo $titulo; ?></span></a></li>
            </ul>

            <div class="inline-form" id="page-principal">
                <?php echo form_open_multipart(site_url('admin/home/edit_people/' . (isset($dataForm) ? $dataForm->id : '0') . '/' . $lang_admin), 'class="crud" id="form-wysiwyg"'); ?>
                <fieldset>
                    <ul>
                        <li>
                            <label for="name">Titulo</label>
                            <div class="input">
                                <?php echo form_input('title', (isset($dataForm->title)) ? $dataForm->title : set_value('title'), 'class="dev-input-title" style="width:100%"'); ?>
                            </div>
                        </li>
                        <li>
                            <label for="name">Texto</label>
                            <div class="input">
                                <textarea name="text" class="wysiwyg-simple"><?php echo (isset($dataForm->text)) ? $dataForm->text : set_value('text');?></textarea>
                            </div>
                        </li>
                    </ul>
                    <?php
                    $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel')));
                    ?>
                </fieldset>
                <?php echo form_close(); ?>
            </div>

        </div>
    </div>
</section>