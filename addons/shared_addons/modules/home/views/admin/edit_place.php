<section class="item">
    <div class="content">
        <h2>Lugares</h2>
        <div class="tabs">
            <ul class="tab-menu">
                <li><a href="#page-banner"><span><?php echo $titulo; ?></span></a></li>
            </ul>
            <div class="form_inputs" id="page-banner">
                <?php echo form_open_multipart(site_url('admin/home/update_place/' . $banner->id . '/' . $banner->id_city), 'class="crud" id="form-wysiwyg"'); ?>
                <div class="inline-form">
                    <fieldset>
                        <ul>
                            <li>
                                <label for="name">Titulo</label>
                                <div class="input"><?php echo form_input('title', $banner->title, 'class="dev-input-title" style="width:100%"'); ?></div>
                            </li>
                            <li>
                                <label for="name">Link</label>
                                <div class="input"><?php echo form_input('link', $banner->link, 'class="dev-input-title" style="width:100%"'); ?></div>
                            </li>
                        </ul>
                        <?php
                        echo form_hidden('id', $banner->id);
                        echo form_hidden('id', $banner->id_city);
                        $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel')));
                        ?>
                    </fieldset>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</section>