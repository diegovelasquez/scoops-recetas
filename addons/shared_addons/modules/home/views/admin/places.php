<section class="title">
    <h4>Ciudades / <?php echo $city->city; ?> / Lugares</h4>
    <a href="<?php echo site_url('admin/home/index/es/#page-numbers') ?>" class="btn small">Volver</a>
</section>
<section class="item">
    <div class="content">
        <div class="tabs">
            <ul class="tab-menu">
                <li><a href="#page-banner"><span>Lugares</span></a></li>
            </ul>

            <!-- BANNER -->
            <div class="form_inputs" id="page-banner">
                <fieldset>
                    <?php echo anchor('admin/home/create_place/'. $city->id .'/'. $lang_admin, '<span>Nuevo</span>', 'class="btn blue"'); ?>
                    <br>
                    <?php if (!empty($places)): ?>

                        <table border="0" class="table-list" cellspacing="0">
                            <thead>
                                <tr>
                                    <th style="width: 40%">Titulo</th>
                                    <th style="width: 40%">Link</th>
                                    <th class="width: 20%">Acciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <div class="inner filtered"><?php $this->load->view('admin/partials/pagination') ?></div>
                                    </td>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($places as $place): ?>
                                    <tr>
                                        <td><?php echo $place->title ?></td>
                                        <td><a href="<?php echo $place->link ?>" target="_blank"><?php echo $place->link ?></a></td>
                                        <td>
                                            <?php echo anchor('admin/home/edit_place/' . $place->id . '/' . $lang_admin, lang('global:edit'), 'class="btn green small"'); ?>
                                            <?php echo anchor('admin/home/delete_place/' . $place->id .'/'. $city->id, lang('global:delete'), array('class' => 'confirm btn red small')) ?>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>

                    <?php else: ?>
                        <p style="text-align: center">No hay un Lugar actualmente</p>
                    <?php endif ?>
                </fieldset>
            </div>

        </div>
    </div>
</section>