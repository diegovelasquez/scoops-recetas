<div id="baseurl" class="hide"><?php echo site_url(); ?></div>
<section class="title">
    <h4>Home</h4>
</section>
<section class="item">
    <div class="content">
        <div class="tabs">
            <ul class="tab-menu">
                <li><a href="#page-banner"><span>Slider</span></a></li>
                <li><a href="#page-destacado"><span>Destacado</span></a></li>
                <li><a href="#page-people"><span>Beneficios</span></a></li>
                <li><a href="#page-banner-customers"><span>Sabores</span></a></li>
                <li><a href="#page-recipes"><span>Recetas</span></a></li>
                <li><a href="#page-numbers"><span>Contacto</span></a></li>
            </ul>

            <!-- BANNER -->
            <div class="form_inputs" id="page-banner">
                <fieldset>
                    <?php echo anchor('admin/home/create_banner/' . $lang_admin, '<span>Nuevo</span>', 'class="btn blue"'); ?>
                    <br>
                    <br>
                    <?php if (!empty($banner)): ?>

                        <table border="0" class="table-list" cellspacing="0">
                            <thead>
                                <tr>
                                    <th style="width: 40%">Imagen</th>
                                    <th style="width: 40%">Titulo</th>
                                    <th class="width: 20%">Acciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <div class="inner filtered"><?php $this->load->view('admin/partials/pagination') ?></div>
                                    </td>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($banner as $slide): ?>
                                    <tr>
                                        <td>
                                            <?php if (!empty($slide->image)): ?>
                                                <img src="<?php echo site_url($slide->image); ?>" style="width: 139px;">
                                            <?php endif; ?>
                                        </td>
                                        <td><?php echo $slide->title ?></td>
                                        <td>
                                            <?php echo anchor('admin/home/edit_banner/' . $slide->id . '/' . $lang_admin, lang('global:edit'), 'class="btn green small"'); ?>
                                            <?php echo anchor('admin/home/delete_banner/' . $slide->id, lang('global:delete'), array('class' => 'confirm btn red small')) ?>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>

                    <?php else: ?>
                        <p style="text-align: center">No hay un slide actualmente</p>
                    <?php endif ?>
                </fieldset>
            </div>

            <!-- DESTACADO -->
            <div class="inline-form" id="page-destacado">
                <fieldset>
                    <?php if (isset($destacado)): ?>
                        <?php echo form_open_multipart(site_url('admin/home/edit_outstanding/' . $lang_admin), 'class="crud" id="form-wysiwyg"'); ?>
                        <fieldset>
                            <ul>
                                <li>
                                    <label for="name">Imagen
                                    </label>
                                    <div class="input">
                                        <?php if (!empty($destacado->image)): ?>
                                            <div>
                                                <img src="<?php echo site_url($destacado->image) ?>" width="298">
                                            </div>
                                        <?php endif; ?>
                                        <div class="btn-false">
                                            <div class="btn">Examinar</div>
                                            <?php echo form_upload('image', '', ' id="image"'); ?>
                                        </div>
                                    </div>
                                    <br class="clear">
                                </li>
                                <li>
                                    <label for="name">Texto</label>
                                    <div class="input">
                                        <?php echo form_textarea(array('id' => 'text', 'name' => 'text', 'value' => (isset($destacado->text)) ? $destacado->text : set_value('text'), 'rows' => 8, 'style' => "width: 100%;height: 100px", 'class' => "wysiwyg-simple")) ?>
                                    </div>
                                </li>
                            </ul>
                            <?php
                            $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel')));
                            ?>
                        </fieldset>
                        <?php echo form_close(); ?>
                    <?php else: ?>
                        <p style="text-align: center">No hay datos actualmente</p>
                    <?php endif ?>
                </fieldset>
            </div>

            <!-- beneficios -->
            <div class="form_inputs" id="page-people">
                <fieldset>
                    <?php if (isset($linkintro)): ?>
                        <?php echo form_open_multipart(site_url('admin/home/edit_link_info/' . $lang_admin), 'class="crud" id="form-wysiwyg"'); ?>
                        <fieldset>
                            <ul>
                                <li>
                                    <label for="name">Titulo</label>
                                    <div class="input"><?php echo form_input('title', (isset($linkintro->title)) ? $linkintro->title : set_value('title'), 'class="dev-input-title"'); ?></div>
                                </li>
                                <li>
                                    <label for="name">Imagen
                                        <small>
                                            - Imagen Permitidas gif | jpg | png | jpeg<br>
                                        </small>
                                    </label>
                                    <div class="input">
                                        <?php if (!empty($linkintro->image)): ?>
                                            <div>
                                                <img src="<?php echo site_url($linkintro->image) ?>" width="298">
                                            </div>
                                        <?php endif; ?>
                                        <div class="btn-false">
                                            <div class="btn">Examinar</div>
                                            <?php echo form_upload('image', set_value('image'), ' id="image"'); ?>
                                        </div>
                                    </div>
                                    <br class="clear">
                                </li>
                            </ul>
                            <?php
                            $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel')));
                            ?>
                        </fieldset>
                        <?php echo form_close(); ?>
                    <?php else: ?>
                        <p style="text-align: center">No hay datos actualmente</p>
                    <?php endif ?>
                </fieldset>
                <fieldset>
                    <?php echo anchor('admin/home/edit_people/', '<span>Crear</span>', 'class="btn blue"'); ?>
                    <br>
                    <br>
                    <?php if (!empty($people)): ?>

                        <table border="0" class="table-list" cellspacing="0">
                            <thead>
                                <tr>
                                    <th style="width: 50%">Titulo</th>
                                    <th style="width: 50%">Acciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <div class="inner filtered"><?php $this->load->view('admin/partials/pagination') ?></div>
                                    </td>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($people as $pe): ?>
                                    <tr>
                                        <td><?php echo $pe->title ?></td>
                                        <td>
                                            <?php echo anchor('admin/home/edit_people/' . $pe->id, lang('global:edit'), 'class="btn blue small"'); ?>
                                            <?php echo anchor('admin/home/delete_people/' . $pe->id, lang('global:delete'), array('class' => 'btn red small confirm button')) ?>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>

                    <?php else: ?>
                        <p style="text-align: center">No hay un slider de clientes actualmente</p>
                    <?php endif ?>
                </fieldset>
            </div>

            <!-- sabores -->
            <div class="form_inputs" id="page-banner-customers">
                <fieldset>
                    <?php if (isset($video)): ?>
                        <?php echo form_open_multipart(site_url('admin/home/edit_video_info/' . $lang_admin), 'class="crud" id="form-wysiwyg"'); ?>
                        <fieldset>
                            <ul>
                                <li>
                                    <label for="title">Titulo</label>
                                    <div class="input">
                                        <?php echo form_input('title', (isset($video->title)) ? $video->title : set_value('title'), 'class="dev-input-url"'); ?>
                                    </div>
                                </li>
                            </ul>
                            <?php
                            $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel')));
                            ?>
                        </fieldset>
                        <?php echo form_close(); ?>
                    <?php else: ?>
                        <p style="text-align: center">No hay datos actualmente</p>
                    <?php endif ?>
                </fieldset>
                <fieldset>
                    <?php echo anchor('admin/home/edit_customers/', '<span>Nuevo</span>', 'class="btn blue"'); ?>
                    <br>
                    <br>
                    <?php if (!empty($customers)): ?>

                        <table border="0" class="table-list" cellspacing="0">
                            <thead>
                                <tr>
                                    <th style="width: 40%">Imagen</th>
                                    <th style="width: 40%">nombre</th>
                                    <th class="width-10">Acciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <div class="inner filtered"><?php $this->load->view('admin/partials/pagination') ?></div>
                                    </td>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($customers as $customer): ?>
                                    <tr>
                                        <td>
                                            <?php if (!empty($customer->image)): ?>
                                                <img src="<?php echo site_url($customer->image); ?>" style="width: 139px;">
                                            <?php endif; ?>
                                        </td>
                                        <td><?php echo $customer->name ?></td>
                                        <td>
                                            <?php echo anchor('admin/home/edit_customers/' . $customer->id, lang('global:edit'), 'class="btn blue small"'); ?>
                                            <?php echo anchor('admin/home/delete_customers/' . $customer->id, lang('global:delete'), array('class' => 'btn red small confirm button')) ?>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>

                    <?php else: ?>
                        <p style="text-align: center">No hay un slider de clientes actualmente</p>
                    <?php endif ?>
                </fieldset>
            </div>

            <!-- recipes -->
            <div class="form_inputs" id="page-recipes">
                <fieldset>
                    <?php if (isset($video)): ?>
                        <?php echo form_open_multipart(site_url('admin/home/edit_video_info/' . $lang_admin), 'class="crud" id="form-wysiwyg"'); ?>
                        <fieldset>
                            <ul>
                                <li>
                                    <label for="title">Titulo</label>
                                    <div class="input">
                                        <?php echo form_input('title_recipes', (isset($video->title_recipes)) ? $video->title_recipes : set_value('title_recipes'), 'class="dev-input-url"'); ?>
                                    </div>
                                </li>
                            </ul>
                            <?php
                            $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel')));
                            ?>
                        </fieldset>
                        <?php echo form_close(); ?>
                    <?php else: ?>
                        <p style="text-align: center">No hay datos actualmente</p>
                    <?php endif ?>
                </fieldset>
                <fieldset>
                    <?php echo anchor('admin/home/edit_recipes/', '<span>Nuevo</span>', 'class="btn blue"'); ?>
                    <br>
                    <br>
                    <?php if (!empty($recipes)): ?>

                        <table border="0" class="table-list" cellspacing="0">
                            <thead>
                                <tr>
                                    <th style="width: 80%">nombre</th>
                                    <th class="width-10">Acciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <div class="inner filtered"><?php $this->load->view('admin/partials/pagination') ?></div>
                                    </td>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($recipes as $recipe): ?>
                                    <tr>
                                        <td><?php echo $recipe->title ?></td>
                                        <td>
                                            <?php echo anchor('admin/home/edit_recipes/' . $recipe->id, lang('global:edit'), 'class="btn blue small"'); ?>
                                            <?php echo anchor('admin/home/delete_recipes/' . $recipe->id, lang('global:delete'), array('class' => 'btn red small confirm button')) ?>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>

                    <?php else: ?>
                        <p style="text-align: center">No hay un slider de clientes actualmente</p>
                    <?php endif ?>
                </fieldset>
            </div>

            <!-- Contacto -->
            <div class="form_inputs" id="page-numbers">
                <fieldset>
                    <?php if (isset($linkintro)): ?>
                        <?php echo form_open_multipart(site_url('admin/home/edit_numbers_info/' . $lang_admin), 'class="crud" id="form-wysiwyg"'); ?>
                        <fieldset>
                            <ul>
                                <li>
                                    <label for="name">Titulo</label>
                                    <div class="input"><?php echo form_input('title2', (isset($linkintro->title2)) ? $linkintro->title2 : set_value('title2'), 'class="dev-input-title"'); ?></div>
                                </li>
                                <li>
                                    <label for="name">Titulo de formulario de Contacto</label>
                                    <div class="input"><?php echo form_input('title3', (isset($linkintro->title3)) ? $linkintro->title3 : set_value('title3'), 'class="dev-input-title"'); ?></div>
                                </li>
                                <li>
                                    <label for="name">Email de Contacto</label>
                                    <div class="input"><?php echo form_input('email', (isset($linkintro->email)) ? $linkintro->email : set_value('email'), 'class="dev-input-title"'); ?></div>
                                </li>
                                <li>
                                    <label for="name">Facebook</label>
                                    <div class="input"><?php echo form_input('facebook', (isset($linkintro->facebook)) ? $linkintro->facebook : set_value('facebook'), 'class="dev-input-title"'); ?></div>
                                </li>
                                <li>
                                    <label for="name">Instagram</label>
                                    <div class="input"><?php echo form_input('instagram', (isset($linkintro->instagram)) ? $linkintro->instagram : set_value('instagram'), 'class="dev-input-title"'); ?></div>
                                </li>
                                <li>
                                    <label for="name">Teléfono</label>
                                    <div class="input"><?php echo form_input('phone', (isset($linkintro->phone)) ? $linkintro->phone : set_value('phone'), 'class="dev-input-title"'); ?></div>
                                </li>
                            </ul>
                            <?php
                            $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel')));
                            ?>
                        </fieldset>
                        <?php echo form_close(); ?>
                    <?php else: ?>
                        <p style="text-align: center">No hay datos actualmente</p>
                    <?php endif ?>
                </fieldset>
                <fieldset>
                    <?php
                    echo anchor('admin/home/edit_numbers/0/' . $lang_admin, '<span>Nuevo</span>', 'class="btn blue"');
                    if ($ordering):
                        echo anchor('admin/home/index/' . $lang_admin . '/#page-numbers', '<span>Volver</span>', 'class="btn blue"');
                    else:
                        echo anchor('admin/home/index/' . $lang_admin . '/1/#page-numbers', '<span>Ordenar</span>', 'class="btn blue"');
                    endif;
                    ?>
                    <br>
                    <br>
                    <?php if (!empty($numbers)): ?>
                        <div id="ajax_message_prod"></div>
                        <table border="0" class="table-list <?= ($ordering) ? 'sort' : ''; ?>" cellspacing="0">
                            <thead>
                                <tr>
                                    <?php if ($ordering): ?>
                                        <th width="4%"></th>
                                    <?php endif; ?>
                                    <th style="width: 70%">Ciudad</th>
                                    <th class="width: 20%">Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="<?= ($ordering) ? 'sortable_numbers' : ''; ?>">
                                <?php foreach ($numbers as $number): ?>
                                    <tr id="<?php echo $number->id ?>">
                                        <?php if ($ordering): ?>
                                            <td>
                                                <span class="handle"></span>   
                                            </td>
                                        <?php endif; ?>
                                        <td><?php echo $number->city ?></td>
                                        <td>
                                            <?php echo anchor('admin/home/edit_numbers/' . $number->id . '/' . $lang_admin, lang('global:edit'), 'class="btn green small"'); ?>
                                            <?php echo anchor('admin/home/places/' . $number->id . '/' . $lang_admin, Lugares, 'class="btn orange small"'); ?>
                                            <?php echo anchor('admin/home/delete_numbers/' . $number->id, lang('global:delete'), array('class' => 'confirm btn red small')) ?>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>

                    <?php else: ?>
                        <p style="text-align: center">No hay elementos actualmente</p>
                    <?php endif ?>
                </fieldset>
            </div>

        </div>
    </div>
</section>