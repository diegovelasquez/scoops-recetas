<section class="item">
    <div class="content">
        <h2>Donde Comprar</h2>
        <div class="tabs">
            <ul class="tab-menu">
                <li><a href="#page-destacado"><span><?php echo $titulo; ?></span></a></li>
            </ul>
            <div class="form_inputs" id="page-bibliografia">
                <?php echo form_open_multipart(site_url('admin/home/edit_numbers/' . (isset($outstanding) ? $outstanding->id : '0') . '/' . $lang_admin), 'class="crud"'); ?>
                <div class="inline-form">
                    <fieldset>
                        <ul>
                            <li>
                                <label for="title">Cuidad</label>
                                <div class="input">
                                    <?php echo form_input('city', (isset($outstanding->city)) ? $outstanding->city : set_value('city'), 'class="dev-input-title"'); ?>
                                </div>
                            </li>
                        </ul>
                    </fieldset>

                    <div class="buttons">
                        <?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))); ?>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>

        </div>
    </div>
</section>