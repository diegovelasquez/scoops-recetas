<section class="item">
    <div class="content">
        <h2>Recetas</h2>
        <div class="tabs">
            <ul class="tab-menu">
                <li><a href="#page-principal"><span><?php echo $titulo; ?></span></a></li>
            </ul>

            <div class="inline-form" id="page-principal">
                <?php echo form_open_multipart(site_url('admin/home/edit_recipes/' . (isset($dataForm) ? $dataForm->id : '0') . '/' . $lang_admin), 'class="crud" id="form-wysiwyg"'); ?>
                <fieldset>
                    <ul>
                        <li>
                            <label for="name">Tipo de video <small>*</small></label>
                            <div class="input">
                                <select name="type" id="type">
                                    <option value="1" <?php echo ($dataForm->type == 1)? 'selected' : '';?>>URL de youtube / Vimeo</option>
                                    <option value="2" <?php echo ($dataForm->type == 2)? 'selected' : '';?>>Video mp4</option>
                                </select>
                            </div>
                        </li>
                        <li id="video" class="<?php echo ($dataForm->type == 2)? '' : 'hide';?>">
                            <label for="name">Video
                                <small>
                                    - Formatos Permitidos mp4<br>
                                </small>
                            </label>
                            <?php if (!empty($dataForm->path)): ?>
                                <div>
                                    <video src="<?php echo site_url($dataForm->path) ?>" width="320" height="240" controls>
                                </div>
                            <?php endif; ?>
                            <div class="btn-false">
                                <div class="btn">Examinar</div>
                                <?php echo form_upload('path', set_value('path'), ' id="path"'); ?>
                            </div>
                        </li>
                        <li id="url" class="<?php echo ($dataForm->type == 1)? '' : 'hide';?>">
                            <label for="name">URL youtube / vimeo</label>
                            <?php if (!empty($dataForm->path)): ?>
                                <div>
                                    <iframe width="320" height="240" src="<?php echo $dataForm->content ?>" frameborder="0" allowfullscreen></iframe>
                                </div>
                            <?php endif; ?>
                            <div class="input">
                                <?php echo form_input('video', (isset($dataForm->video)) ? $dataForm->video : set_value('video'), 'class="dev-input-title" style="width:100%"'); ?>
                            </div>
                        </li>
                        <li>
                            <label for="name">Titulo</label>
                            <div class="input">
                                <?php echo form_input('title', (isset($dataForm->title)) ? $dataForm->title : set_value('title'), 'class="dev-input-title" style="width:100%"'); ?>
                            </div>
                        </li>
                        <li>
                            <label for="name">Ingredientes</label>
                            <div class="input">
                                <textarea id="ingredients-wysiwyg" class="wysiwyg-simple"><?php echo (isset($dataForm->ingredients)) ? $dataForm->ingredients : set_value('ingredients') ?></textarea>
                                <input type="hidden" name="ingredients" id="ingredients">
                            </div>
                        </li>
                        <li>
                            <label for="name">Preparación</label>
                            <div class="input">
                                <textarea id="preparation-wysiwyg" class="wysiwyg-simple"><?php echo (isset($dataForm->preparation)) ? $dataForm->preparation : set_value('preparation') ?></textarea>
                                <input type="hidden" name="preparation" id="preparation">
                            </div>
                        </li>
                    </ul>
                    <?php
                    $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel')));
                    ?>
                </fieldset>
                <?php echo form_close(); ?>
            </div>

        </div>
    </div>
</section>
<script>
$('#type').change(function (){
    var tipo = $(this).val();
    
    if(tipo == 1){
        $('#video').addClass('hide');
        $('#url').removeClass('hide');
    }else{
        $('#url').addClass('hide');
        $('#video').removeClass('hide');
    }
    
});
</script>