/*
 * @author Luis Fernando Salazar
 * @description 
 */

$(document).ready(function () {
initialize();
});

function initialize() {
    var base_url = $('#baseurl').html();  // seleccionamos la base url de un div  
    var map;
    var lat1 = $('#lat').html();
    var lng1 = $('#lng').html();
        
    var map = new google.maps.Map(document.getElementById('map_canvas'), {
        center: new google.maps.LatLng(lat1, lng1),
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    });

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat1, lng1),
        map: map
    });
    
}
